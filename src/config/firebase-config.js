// Import the functions you need from the SDKs you need
import firebase from "firebase/compat";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDuK5UebqOcw9VBIMq4yiNJolOHSgIUleg",
    authDomain: "reactproject-9d0ed.firebaseapp.com",
    projectId: "reactproject-9d0ed",
    storageBucket: "reactproject-9d0ed.appspot.com",
    messagingSenderId: "330768601105",
    appId: "1:330768601105:web:37f55dca668684bd4ae1b0",
    measurementId: "G-GHZCC93FEN"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

export default firebase;
