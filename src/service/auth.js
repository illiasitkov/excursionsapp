import firebase from "../config/firebase-config";

const googleAuthentication = (provider) => {
    return firebase.auth().signInWithPopup(provider)
        .then((res) => {
            return res.user;
        })
        .catch((err) => {
            return err;
        });
}

export default googleAuthentication;



