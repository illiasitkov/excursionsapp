import React from 'react';
import {TileLayer, MapContainer, Marker, Popup, useMap} from 'react-leaflet';

const MapComponent = ({style, position, markerLabel}) => {
    const RenderMarker = ({position, markerLabel}) => {
        const map = useMap();
        return (
            <Marker position={position} eventHandlers={{
                click: () => {
                    map.setView(position, 15);
                }
            }}>
                <Popup>
                    {markerLabel}
                </Popup>
            </Marker>
        );
    }

    return (
        <MapContainer style={style} center={position} zoom={13} scrollWheelZoom={false}>
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
            <RenderMarker markerLabel={markerLabel} position={position}/>
        </MapContainer>
    );
}

export default MapComponent;
