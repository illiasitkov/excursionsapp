import React from "react";

const TopBadge = ({show, text}) => {
    return (
        <>
            {show && <span>&nbsp;<span className='badge-style badge badge-success'> {text} </span></span>}
        </>
    );
}

export default TopBadge;
