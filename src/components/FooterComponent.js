import React from 'react';
import '../css/footer.css';
import {Link} from 'react-router-dom';

const Footer = () => {
    return (
        <div className='footer'>
            <div className='container'>
                <div className='row'>
                    <div className='col-4 col-sm-2'>
                        <h5>Навігація</h5>
                        <ul className='list-unstyled'>
                            <li><Link to='/home'>Головна</Link></li>
                            <li><Link to='/excursions'>Екскурсії</Link></li>
                            <li><Link to='/contacts'>Контакти</Link></li>
                            <li><Link to='/gallery'>Галерея</Link></li>
                        </ul>
                    </div>
                    <div className='col-7 col-sm-5'>
                        <h5>Зв'язатись з нами</h5>
                        <address>
                            буд. 2, вул. Горького,<br/>
                            м. Часів Яр,<br/>
                            Донецька область, Україна<br/>
                            <i className='fa fa-phone fa-lg'></i> +38 067 85 0809 05<br/>
                            <i className='fa fa-envelope fa-lg'></i> <a href='mailto:illiasitkov@gmail.com'>
                            напишіть нам</a>
                        </address>
                    </div>
                    <div className='col-12 col-sm-4 align-self-center'>
                        <div>
                            <a className='btn' href='https://www.instagram.com/chasiviar/'><i
                                className='fa fa-lg fa-instagram'></i></a>
                            <a className='btn' href='http://www.facebook.com'><i
                                className='fa fa-lg fa-facebook'></i></a>
                            <a className='btn' href='http://youtube.com/'><i className='fa fa-lg fa-youtube'></i></a>
                            <a className='btn' href='mailto:illiasitkov@gmail.com'><i
                                className='fa fa-lg fa-envelope-o'></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;
