import React from 'react';
import '../css/gallery.css';
import BlankPage from "./BlankPageComponent";

const Gallery = ({excursions, loadingExcursions}) => {
    const photos = excursions.map((e) => {
        return (
            <div key={e.src} className='mb-5'>
                <div className='img-wrap-gallery mb-2'>
                    <img className='img-gallery' src={e.src} alt={e.caption}/>
                </div>
                <div>{e.caption}</div>
            </div>
        );
    });
    return (
        <div className='container mt-5 mb-5'>
            <div className='row'>
                <h1 className='col-12'>Галерея</h1>
            </div>
            <hr/>
            <BlankPage condition={loadingExcursions} loadingSpinner={true} message='Завантаження...'/>
            <div className='row  justify-content-around'>
                {photos}
            </div>
        </div>
    );
}

export default Gallery;
