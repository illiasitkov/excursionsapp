import React, {Component} from 'react';
import {Carousel, CarouselCaption, CarouselControl, CarouselIndicators, CarouselItem} from "reactstrap";
import '../css/carousel.css';
import {Link} from "react-router-dom";

class CarouselComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            slideId: 0,
            animating: false
        };

        this.nextSlide = this.nextSlide.bind(this);
        this.prevSlide = this.prevSlide.bind(this);
        this.moveToSlide = this.moveToSlide.bind(this);
        this.onExited = this.onExited.bind(this);
        this.onExiting = this.onExiting.bind(this);
    }

    nextSlide() {
        if (this.state.animating) return;
        this.setState({
            slideId: (this.state.slideId + 1) % this.props.excursions.length
        });
    }

    prevSlide() {
        if (this.state.animating) return;
        let itemId = this.state.slideId - 1;
        if (itemId < 0) {
            itemId = this.props.excursions.length - 1;
        }
        this.setState({
            slideId: itemId
        });
    }

    moveToSlide(slideId) {
        if (this.state.animating) return;
        this.setState({
            slideId: slideId
        });
    }

    onExiting() {
        this.setState({
            animating: true
        });
    }

    onExited() {
        this.setState({
            animating: false
        });
    }

    render() {
        const items = this.props.excursions.map((item) => {
            return (
                <CarouselItem
                    onExiting={this.onExiting}
                    onExited={this.onExited}
                    key={item.src}
                    className='carousel-item-style'>
                    <img draggable='false' className='carousel-img' src={item.src} alt={item.caption}/>
                    <Link to={`/excursions/${item.id}`}>
                        <CarouselCaption className='caption-style' captionText={item.text}
                                         captionHeader={item.caption}/>
                    </Link>
                </CarouselItem>
            );
        });
        return (
            <div>
                <Carousel interval={4000} previous={this.prevSlide} next={this.nextSlide}
                          activeIndex={this.state.slideId}>
                    <CarouselIndicators items={this.props.excursions} activeIndex={this.state.slideId}
                                        onClickHandler={this.moveToSlide}/>
                    {items}
                    <CarouselControl direction='prev' directionText='Previous' onClickHandler={this.prevSlide}/>
                    <CarouselControl direction='next' directionText='Next' onClickHandler={this.nextSlide}/>
                </Carousel>
            </div>
        );
    }
}

export default CarouselComponent;
