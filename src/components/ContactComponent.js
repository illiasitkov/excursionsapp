import React from 'react';
import MapComponent from './MapComponent';
import '../css/contacts.css';

const Contact = () => {
    return (
        <div className='container mt-5 mb-5'>
            <div className='row'>
                <h1 className='col-12 col-md-6'>Контакти</h1>
            </div>
            <hr/>
            <div className='row'>
                <div className='col-12 col-md-6 mb-4'>
                    <h3>Де ми знаходимось</h3>
                    <h5>Адреса</h5>
                    <address>
                        буд. 2, вул. Горького,<br/>
                        м. Часів Яр,<br/>
                        Донецька область, Україна<br/>
                        <i className='fa fa-phone fa-lg'></i> +38 067 85 0809 05<br/>
                        <i className='fa fa-envelope fa-lg'></i> <a href='mailto:illiasitkov@gmail.com'> напишіть
                        нам</a>
                    </address>
                    <div>
                        <div className='btn-group' role='group'>
                            <a role='button' className='btn btn-primary btn-contact-call' href='tel:+85212345678'><i
                                className='fa fa-phone'></i> Зателефонувати</a>
                            <a role='button' className='btn btn-success btn-contact-email'
                               href='mailto:illiasitkov@gmail.com'><i className='fa fa-envelope-o'></i> Написати</a>
                        </div>
                    </div>
                </div>
                <div className='col-12 col-md-6'>
                    <MapComponent position={[48.588431, 37.832946]} markerLabel='Екскурсійне бюро "Around"'
                                  style={{width: '100%', height: 400}}/>
                </div>
            </div>
        </div>
    );
}

export default Contact;
