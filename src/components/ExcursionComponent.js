import React, {Component} from 'react';
import {Breadcrumb, BreadcrumbItem, Button, Card, CardBody, CardHeader, CardImg} from "reactstrap";
import {Link} from "react-router-dom";
import '../css/excursion.css';
import PillsComponent from "./PillsComponent";
import MapComponent from "./MapComponent";
import BlankPage from "./BlankPageComponent";
import dayjs from 'dayjs';
import {availableExcursion, expiredExcursion} from "./excursion_functions";
import TopBadge from "./TopBadge";

function RenderDescription({excursion}) {
    return (
        <div className='col-md-5 col-12 '>
            <p className='subtitle'>{excursion.description}</p>
            <p className='full-text'>{excursion.full_text}</p>
        </div>
    );
}

const RenderPhoto = ({photoUrl, alt}) => {
    const url = photoUrl;
    return (
        <div className='col-md-7 col-12'>
            <CardImg id='image1' object='true' src={(url.startsWith('http') ? '' : 'http://localhost:3000/') + url} alt={alt}/>
        </div>
    );
}

function RenderInfo({excursion, takeExcursion, refuseExcursion, user}) {
    const RenderButton = ({excursion, user, takeExcursion, refuseExcursion}) => {
        const noFreePlaces = !availableExcursion(excursion);
        const hasExpired = expiredExcursion(excursion);
        const userHasExcursion = user && user.excursionsRegistered && user.excursionsRegistered.includes(excursion.id);

        if (hasExpired) {
            return (
                <p>Запис на екскурсію закінчено</p>
            );
        } else if (!user) {
            return (
                <p>Щоб записатися на екскурсію, увійдіть у свій обліковий запис</p>
            );
        } else if (noFreePlaces && !userHasExcursion) {
            return (
                <p>Вільних місць для запису немає</p>
            );
        } else if (userHasExcursion) {
            return (
                <Button onClick={() => refuseExcursion(excursion)}
                        className='btn-danger mb-3 btn-refuse-excursion'>Виписатися</Button>
            );
        } else {
            return (
                <Button onClick={() => takeExcursion(excursion)}
                        className='btn-success mb-3 btn-take-excursion'>Записатися</Button>
            );
        }
    }
    return (
        <div className='col-12 mt-5 mb-5'>
            <Card className='card-info'>
                <CardHeader className='card-header-style'>
                    Деталі екскурсії
                </CardHeader>
                <CardBody className='row'>
                    <div className='col-12 col-md-6'>
                        <dl className='row'>
                            <dt className="col-6">Місце розташування</dt>
                            <dd className="col-6">{excursion.text}</dd>
                            <dt className="col-6">Дата та час проведення</dt>
                            <dd className="col-6">{dayjs(excursion.date_start).format('DD.MM.YYYY HH:mm')}</dd>
                            <dt className="col-6">Кількість місць</dt>
                            <dd className="col-6">{excursion.max_users}</dd>
                            <dt className="col-6">Кількість вільних місць</dt>
                            <dd className="col-6">{excursion.max_users - (excursion.user_ids ? excursion.user_ids.length : 0)}</dd>
                            <dt className="col-6">Ціна</dt>
                            <dd className="col-6">{excursion.price} грн.</dd>
                            <dt className="col-6">Тривалість</dt>
                            <dd className="col-6">{excursion.duration} годин</dd>
                            <dt className="col-6">Доступна в містах</dt>
                            <dd className="col-6">{excursion.cities.join(', ')}</dd>
                        </dl>
                        <hr/>
                        <RenderButton excursion={excursion} refuseExcursion={refuseExcursion}
                                      takeExcursion={takeExcursion} user={user}/>
                    </div>
                    <div className='col-12 col-md-6'>
                        <MapComponent markerLabel={excursion.caption}
                                      position={[excursion.latitude, excursion.longitude]}
                                      style={{width: '100%', height: 400}}/>
                    </div>
                </CardBody>
            </Card>
        </div>
    );
}

class ExcursionComponent extends Component {

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    getPageName(pageLink) {
        switch (pageLink) {
            case '/home':
                return 'Головна';
            case '/excursions':
                return 'Екскурсії';
            case '/myexcursions':
                return 'Мої екскурсії';
            default:
                return 'Попередня';
        }
    }

    render() {
        if (this.props.loadingExcursions) {
            return (
                <BlankPage loadingSpinner={true} condition={true} message='Завантаження...'/>
            );
        } else if (!this.props.excursion) {
            return (
                <BlankPage loadingSpinner={false} condition={true} message='Дані про екскурсію не знайдено!'/>
            );
        }
        const pageFrom = this.props.pageFrom;
        const pageName = this.getPageName(pageFrom);
        return (
            <div className='container'>
                <div className='row mt-3'>
                    <Breadcrumb className='breadcrumb-style'>
                        {pageFrom === '/myexcursions' &&
                        <BreadcrumbItem><Link to='/excursions'>Екскурсії</Link></BreadcrumbItem>}
                        <BreadcrumbItem><Link to={pageFrom}>{pageName}</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{this.props.excursion.caption}</BreadcrumbItem>
                    </Breadcrumb>
                    <h1 className='col-12'>{this.props.excursion.caption}<TopBadge show={this.props.excursion.top}
                                                                                   text='ТОП'/></h1>
                </div>
                <div className='row align-items-center'>
                    <div className='col-md-5 col-12'>
                        <PillsComponent excursion={this.props.excursion}/>
                    </div>
                </div>
                <hr/>
                <div className='row mb-5'>
                    <RenderDescription excursion={this.props.excursion}/>
                    <RenderPhoto photoUrl={this.props.excursion.src} alt={this.props.excursion.caption}/>
                </div>
                <hr/>
                <div className='row'>
                    <RenderInfo user={this.props.user} excursion={this.props.excursion}
                                takeExcursion={this.props.takeExcursion} refuseExcursion={this.props.refuseExcursion}/>
                </div>
            </div>
        );
    }
}

export default ExcursionComponent;
