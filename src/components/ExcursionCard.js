import React from "react";
import {Button, Card, CardBody, CardFooter, CardImg, CardText, CardTitle} from "reactstrap";
import PillsComponent from "./PillsComponent";
import dayjs from "dayjs";
import TopBadge from "./TopBadge";

const ExcursionCard = ({excursion, history, extended, pageFrom}) => {
    return (
        <Card className='top-excursion-card'>
            <div className='top-excursion-card-img-wrapper'>
                <CardImg className='top-excursion-card-img' draggable={false} object='true' src={excursion.src} alt={excursion.caption}/>
            </div>
            <CardBody>
                <CardTitle className='top-excursion-card-title'>{excursion.caption}<TopBadge
                    show={excursion.top} text='ТОП'/></CardTitle>
                <CardText>
                    {excursion.description}<br/>
                </CardText>
                <hr/>
                {extended ?
                    <>
                        <CardText className='price-date-style mt-2'>
                                {excursion.text}<br/>
                                Вартість: {excursion.price} грн.<br/>
                                Дата та час: {dayjs(excursion.date_start).format('DD.MM.YYYY HH:mm')}<br/>
                                Тривалість: {excursion.duration} год.
                        </CardText>
                        <hr/>
                    </>:
                    <CardText className='price-date-style mt-2'>
                        {excursion.text}
                    </CardText>
                }
                <CardText>
                    <PillsComponent excursion={excursion}/>
                </CardText>
            </CardBody>
            <CardFooter className='top-excursion-card-footer'>
                <Button
                    onClick={() => history.push(`excursions/${excursion.id}`, {from: pageFrom})}
                    className='details-button'>Дізнатися більше</Button>
            </CardFooter>
        </Card>
    );
}

export default ExcursionCard;
