import React from 'react';
import {Spinner} from "reactstrap";

const BlankPage = ({condition, message, loadingSpinner}) => {
    if (!condition) {
        return null;
    }
    return (
        <>
            <div className='container mt-5 mb-5 no-access-warning'>
                <div className='row justify-content-center align-items-center'>
                    {message}
                    {loadingSpinner &&
                    <Spinner className='ml-4' animation="border"/>}
                </div>

            </div>
        </>
    );
}

export default BlankPage;
