import React from "react";
import '../css/pills.css'

function PillsComponent(props) {

    function getCategoryClass(category) {
        switch (category) {
            case 'велопрогулянка':
                return 'cycle-pill';
            case 'прогулянка':
                return 'walk-pill';
            case 'автобусна екскурсія':
                return 'bus-pill';
            case 'мистецтво':
                return 'art-pill';
            case 'виробництво':
                return 'production-pill';
            case 'еко-стежка':
                return 'eco-pill';
            default:
                return '';
        }
    }
    const pills = props.excursion.categories.map((category) => {
        return (
            <span key={category} className={getCategoryClass(category)}>{category}</span>
        );
    });
    return (
        <span className='pills'>
            {pills}
        </span>);
}

export default PillsComponent;
