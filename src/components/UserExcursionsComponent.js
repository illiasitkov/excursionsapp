import React from "react";
import '../css/excursion_manu.css';
import ExcursionCard from "./ExcursionCard";
import {Breadcrumb, BreadcrumbItem} from "reactstrap";
import {Link} from "react-router-dom";
import '../css/user_excursions.css';
import BlankPage from "./BlankPageComponent";

const UserExcursionsComponent = ({excursions, history, userLoggedOut}) => {
    const myExcursions = excursions.map(e => {
        return (
            <div key={e.src} className='mb-5 col-12 col-md-6 col-lg-4'>
                <ExcursionCard history={history} extended={true} pageFrom='/myexcursions' excursion={e}/>
            </div>
        );
    });
    return (
        <>
            <div className='container'>
                <div className='row mt-3'>
                    <Breadcrumb className='breadcrumb-style'>
                        <BreadcrumbItem><Link to='/excursions'>Екскурсії</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Мої екскурсії</BreadcrumbItem>
                    </Breadcrumb>
                    <h1 className='col-12'>Мої екскурсії</h1>
                </div>
                <hr/>
                <BlankPage condition={userLoggedOut} loadingSpinner={false}
                           message='Щоб переглянути список ваших екскурсій, потрібно ввійти в обліковий запис'/>
                <BlankPage condition={!userLoggedOut && myExcursions.length === 0} loadingSpinner={false}
                           message='Поки що список ваших екскурсій порожній. Для запису перейдіть до екскурсій або на головну сторінку'/>
                <div className='row'>
                    {myExcursions}
                </div>
            </div>
        </>
    );
}

export default UserExcursionsComponent;
