import React, {Component} from "react";
import {Button, Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem} from "reactstrap";
import '../css/header.css';
import {Link, NavLink, withRouter} from "react-router-dom";
import {NavDropdown} from "react-bootstrap";

const RenderLoginInfo = ({user, signIn, signOut}) => {
    if (user != null) {
        console.log(user);
        return (
            <div className='ml-auto user-login-info'>
                <Link to='/myexcursions'>
                    <div className='user-avatar mr-1'>
                        <img src={user.photoURL} alt={user.name}/>
                    </div>
                </Link>
                <Link to='/myexcursions'>
                    <span className='mr-4 user-name'>{user.userName}</span>
                </Link>
                <Button className='btn-sign' onClick={() => signOut()}>Вийти</Button>
            </div>
        );
    } else {
        return (
            <Button className='ml-5 ml-md-auto btn-sign' onClick={() => signIn()}>Увійти</Button>
        );
    }
}

const RenderSearchForm = ({handleHeaderSearch}) => {
    return (
        <form action='/excursions' className='form-inline ml-3 mb-3 mb-md-0' onSubmit={(e) => handleHeaderSearch(e)}>
            <input className='search-input-header pl-1' type='text' name='searchString' placeholder='Шукати...'/>
        </form>
    );
}

class HeaderComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isNavOpen: false
        };

        this.toggleNav = this.toggleNav.bind(this);
        this.moveToPage = this.moveToPage.bind(this);
    }

    toggleNav() {
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }

    moveToPage(toPage) {
        this.props.history.push(toPage);
    }

    render() {
        return (
            <>
                <Navbar className='navigation-bar fixed-top navbar-light' expand='lg'>
                    <div className='container'>
                        <NavbarToggler dark='true' onClick={this.toggleNav}/>
                        <NavbarBrand className='mr-auto ml-5 ml-lg-0' href='/'>Around</NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar className='ml-5'>
                                <NavItem>
                                    <NavLink className='nav-link border-right border-secondary' to='/home'>
                                        Головна
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink className='nav-link border-right border-secondary' to='/excursions'>
                                        Екскурсії
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavDropdown className='border-right border-secondary' title='Про нас'
                                                 menuVariant="light">
                                        <NavDropdown.Item onClick={() => {
                                            this.moveToPage('/contacts');
                                        }}>Контакти</NavDropdown.Item>
                                        <NavDropdown.Item onClick={() => {
                                            this.moveToPage('/gallery');
                                        }}>Галерея</NavDropdown.Item>
                                    </NavDropdown>
                                </NavItem>
                            </Nav>
                            <RenderSearchForm handleHeaderSearch={this.props.handleHeaderSearch}/>
                            <RenderLoginInfo user={this.props.user} userSignedIn={this.props.userSignedIn}
                                             signIn={this.props.signIn} signOut={this.props.signOut}/>
                        </Collapse>
                    </div>
                </Navbar>
                <div className='pseudo-header'></div>
            </>
        );
    }
}

export default withRouter(HeaderComponent);
