import React from 'react';
import CarouselComponent from "./CarouselComponent";
import TopExcursionsComponent from "./TopExcursionsComponent";
import '../css/home.css';
import BlankPage from "./BlankPageComponent";

function RenderGeneralInfo() {
    return (
        <div className='container mt-5 mb-5'>
            <div className='row justify-content-center'>
                <h1>Екскурсійне бюро "Around"</h1>
            </div>
            <hr/>
            <div id='generalInfo'>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;Компанія надає професійні послуги з організації та проведення екскурсій більш
                    ніж у 20
                    областях України.
                    <br/>&nbsp;&nbsp;&nbsp;&nbsp;Кожен: і затятий мандрівник-дослідник, і поціновувач мистецтва,
                    і еко-турист, і спортсмен, і любитель смачної їжі неодмінно знайде екскурсію до душі,
                    а досвідчені й кваліфіковані екскурсоводи з радістю розкажуть неймовірні історії про старовинні
                    замки
                    або раритетні автівки.</p>
            </div>
        </div>
    );
}

const Home = ({loadingExcursions, excursions, history}) => {
    if (loadingExcursions) {
        return (
            <div>
                <BlankPage loadingSpinner={true} condition={loadingExcursions} message='Завантаження...'/>
            </div>);
    } else {
        return (
            <>
                <CarouselComponent excursions={excursions}/>
                <RenderGeneralInfo/>
                <TopExcursionsComponent history={history} excursions={excursions}/>
            </>
        );
    }

}

export default Home;
