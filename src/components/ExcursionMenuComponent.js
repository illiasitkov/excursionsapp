import React, {Component} from "react";
import {Button} from "reactstrap";
import '../css/excursion_manu.css';
import ExcursionCard from "./ExcursionCard";
import {Link} from "react-router-dom";
import BlankPage from "./BlankPageComponent";
import {availableExcursion, excursionHasString, excursionIsOnDate, sortExcursionsByParam} from "./excursion_functions";
import {Pagination} from "react-bootstrap";

const PaginationComponent = ({
                                 pageActive,
                                 itemsPerPage,
                                 items,
                                 moveFirst,
                                 moveLast,
                                 moveNext,
                                 movePrev,
                                 moveToPage
                             }) => {
    const numberOfPages = Math.ceil(items.length / itemsPerPage);
    const links = [];

    const movePrevPage = () => {
        if (pageActive !== 1) {
            movePrev();
        }
    }

    function moveNextPage() {
        if (pageActive !== numberOfPages) {
            moveNext();
        }
    }

    links.push(
        <Pagination.First key={'pageFirst'} disabled={pageActive === 1} onClick={() => moveFirst()}>{'<<'}</Pagination.First>,
        <Pagination.Prev key={'pagePrev'} disabled={pageActive === 1} onClick={() => movePrevPage()}>{'<'}</Pagination.Prev>);

    for (let number = 1; number <= numberOfPages; number++) {
        links.push(
            <Pagination.Item onClick={() => moveToPage(number)} key={number} active={number === pageActive}>
                {number}
            </Pagination.Item>,
        );
    }

    links.push(
        <Pagination.Next key={'pageNext'} disabled={pageActive === numberOfPages} onClick={() => moveNextPage()}>{'>'}</Pagination.Next>,
        <Pagination.Last key={'pageLast'} disabled={pageActive === numberOfPages}
                         onClick={() => moveLast(numberOfPages)}>{'>>'}</Pagination.Last>);
    return (
        <div>
            <Pagination>{links}</Pagination>
        </div>
    );
}

function RenderFilter({excursions, handleOnChange, handleOnSearchSubmit, handleOnSortChange}) {
    let cities = [];
    let categories = [];
    excursions.forEach(e => {
        cities = cities.concat(e.cities);
        categories = categories.concat(e.categories);
    });
    cities = new Set(cities);
    categories = new Set(categories);
    const citiesOptions = [...cities].map(c => {
        return (
            <option key={c} value={c}>{c}</option>
        );
    });
    const categoriesOptions = [...categories].map(c => {
        return (
            <option key={c} value={c}>{c}</option>
        );
    });
    return (
        <details className='mb-3'>
            <summary>Параметри пошуку</summary>
            <div className='row mt-3 filter-search'>
                <form onSubmit={(e) => handleOnSearchSubmit(e)} className='col-lg-4 col-12 mb-3 mb-lg-0'>
                    <div className='container pt-3 pr-3 pl-3 mb-1 excursions-filter'>
                        <div className='row search-input-row align-items-center'>
                            <div className='form-group col-8'>
                                <input className='search-input pl-2' type='text' placeholder='Шукати екскурсію...'
                                       name='searchInput' id='searchInput'/>
                            </div>
                            <div className='form-group ml-auto mr-3'>
                                <Button className='btn-submit btn-info' type='submit'>Шукати</Button>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='form-group col-12'>
                                <label className='mr-2' htmlFor='sortBy'>Сортувати:</label>
                                <select onChange={(e) => handleOnSortChange(e)} name='sortBy' id='sortBy'>
                                    <option value='asc-date_start'>За датою (скоро)</option>
                                    <option value='des-date_start'>За датою (нескоро)</option>
                                    <option value='asc-price'>За зростанням ціни</option>
                                    <option value='des-price'>За спаданням ціни</option>
                                    <option value='asc-duration'>За збільшенням тривалості</option>
                                    <option value='des-duration'>За зменшенням тривалості</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
                <form className='col-lg-8 col-12'>
                    <div className='container pt-3 pr-3 pl-3 mb-1 excursions-filter '>
                        <div className='row'>
                            <div className='form-group col-6'>
                                <label className='mr-2' htmlFor='cityFilter'>Місто відправлення:</label>
                                <select onChange={(e) => handleOnChange(e)} name='cityFilter' id='cityFilter'>
                                    <option key='allCities' value=''>Усі</option>
                                    {citiesOptions}
                                </select>
                            </div>

                            <div className='form-group col-lg-6 col-12'>
                                <label className='mr-2' htmlFor='categoryFilter'>Категорія екскурсій:</label>
                                <select onChange={(e) => handleOnChange(e)} name='categoryFilter' id='categoryFilter'>
                                    <option key='allCategories' value=''>Усі</option>
                                    {categoriesOptions}
                                </select>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='form-group col-lg-6 col-12'>
                                <label className='mr-2' htmlFor='categoryFilter'>Дата:</label>
                                <input onChange={(e) => handleOnChange(e)} type='date' name='dateFilter'
                                       id='dateFilter'/>
                            </div>

                            <div className='form-group col-lg-6 col-12'>
                                <label className='mr-2' htmlFor='availableFilter'>Доступні для запису:</label>
                                <input onChange={(e) => handleOnChange(e)} type='checkbox' name='availableFilter'
                                       id='availableFilter'/>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </details>
    );
}

class ExcursionMenuComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            excursions: [],
            filter: {
                cityFilter: '',
                availableFilter: false,
                dateFilter: '',
                categoryFilter: '',
            },
            sortBy: 'asc-date_start',
            searchString: '',
            pageActive: 1,
            itemsPerPage: 6
        }

        this.handleOnFilterChange = this.handleOnFilterChange.bind(this);
        this.handleOnSearchSubmit = this.handleOnSearchSubmit.bind(this);
        this.handleOnSortChange = this.handleOnSortChange.bind(this);
        this.modifyExcursionsArray = this.modifyExcursionsArray.bind(this);
        this.applyFiltering = this.applyFiltering.bind(this);
        this.applySorting = this.applySorting.bind(this);
        this.applySearching = this.applySearching.bind(this);

        this.moveToPage = this.moveToPage.bind(this);
        this.moveLast = this.moveLast.bind(this);
        this.moveFirst = this.moveFirst.bind(this);
        this.movePrev = this.movePrev.bind(this);
        this.moveNext = this.moveNext.bind(this);

        this.getExcursionsPerPage = this.getExcursionsPerPage.bind(this);
    }

    componentDidMount() {
        const s = decodeURI(this.props.searchString);
        this.setState({
            excursions: this.props.excursions.slice(0),
            searchString: s ? s : ''
        }, () => {
            if (s) {
                this.applySearching()
                    .then(this.applySorting);
            } else {
                this.applySorting();
            }
        });
    }

    handleOnSortChange(event) {
        const target = event.target;
        const name = target.id;
        let value = target.value;
        this.setState({
            [name]: value
        }, () => {
            this.applySorting();
        });
    }


    handleOnFilterChange(event) {
        const target = event.target;
        const name = target.id;
        let value = target.value;
        if (name === 'availableFilter') {
            value = target.checked;
        }
        this.setState({
            filter: {
                ...this.state.filter,
                [name]: value
            }
        }, () => {
            console.log('filter after it has been changed: ', this.state.filter);
            this.modifyExcursionsArray();
        });
    }

    handleOnSearchSubmit(event) {
        event.preventDefault();
        const target = event.target;
        const string = target.elements['searchInput'].value;
        this.setState({
            searchString: string
        }, () => {
            this.modifyExcursionsArray();
        });
    }

    modifyExcursionsArray() {
        this.applyFiltering()
            .then(() => {
                return this.applySearching();
            })
            .then(() => {
                return this.applySorting();
            });
    }

    applySearching() {
        const searchString = this.state.searchString;
        if (searchString) {
            const excursions = this.state.excursions.filter((e) => excursionHasString(e, searchString));
            return new Promise((resolve) => {
                this.setState({
                    excursions: excursions,
                    pageActive: 1
                }, () => {
                    resolve();
                });
            });
        }
        return new Promise((resolve) => {
            resolve();
        });
    }

    applySorting() {
        const sortBy = this.state.sortBy;
        const descendingOrder = sortBy.startsWith('des');
        const sortParam = sortBy.split('-')[1];
        console.log('sorting excursions by: ' + sortParam);
        return new Promise((resolve) => {
            this.setState({
                excursions: sortExcursionsByParam(this.state.excursions, sortParam, descendingOrder),
                pageActive: 1
            }, () => {
                resolve();
            });
        });

    }

    applyFiltering() {
        console.log('applying filtering...');
        console.log('with filter ', this.state.filter);
        const checkObject = (excursion, filter, filterParam) => {
            const param = filter[filterParam];
            console.log('filter param value: ' + param);
            switch (filterParam) {
                case 'cityFilter':
                    return excursion.cities.includes(param);
                case 'availableFilter':
                    return availableExcursion(excursion);
                case 'categoryFilter':
                    return excursion.categories.includes(param);
                case 'dateFilter':
                    return excursionIsOnDate(excursion, param);
                default:
                    return false;
            }
        }
        const filterObject = this.state.filter;
        let excursions = this.props.excursions;
        for (let filter in filterObject) {
            if (!!filterObject[filter]) {
                excursions = excursions.filter((e) => checkObject(e, filterObject, filter));
            }
        }
        return new Promise((resolve) => {
            this.setState({
                excursions: excursions,
                pageActive: 1
            }, () => {
                resolve();
            });
        });
    }

    moveNext() {
        this.setState({
            pageActive: this.state.pageActive + 1
        });
    }

    moveLast(last) {
        this.setState({
            pageActive: last
        });
    }

    movePrev() {
        this.setState({
            pageActive: this.state.pageActive - 1
        });
    }

    moveFirst() {
        this.setState({
            pageActive: 1
        });
    }

    moveToPage(toPage) {
        this.setState({
            pageActive: toPage
        });
    }

    getExcursionsPerPage() {
        const excursionsToShow = [];
        const excursions = this.state.excursions;
        const page = this.state.pageActive;
        const perPage = this.state.itemsPerPage;
        for (let i = (page - 1) * perPage; i < page * perPage && i < excursions.length; i++) {
            excursionsToShow.push(excursions[i]);
        }
        return excursionsToShow;
    }

    render() {
        const excursionsToShow = this.getExcursionsPerPage();
        const menu = excursionsToShow.map(e => {
            return (
                <div key={e.src} className='mb-5 col-12 col-md-6 col-lg-4'>
                    <ExcursionCard history={this.props.history} extended={true} pageFrom='/excursions' excursion={e}/>
                </div>
            );
        });
        return (
            <>
                <div className='container mt-5 mb-5'>
                    <div className='row align-items-center justify-content-between'>
                        <h1 className='col-sm-6 col-12'>Екскурсії</h1>
                        {this.props.userSignedIn ? <Link to='/myexcursions'>
                            <Button className='ml-3 mr-3 btn-my-excursions'>Мої екскурсії</Button>
                        </Link> : null}
                    </div>
                    <hr/>

                    <RenderFilter handleOnSearchSubmit={this.handleOnSearchSubmit}
                                  handleOnChange={this.handleOnFilterChange}
                                  handleOnSortChange={this.handleOnSortChange} excursions={this.props.excursions}/>

                    <BlankPage condition={this.props.loadingExcursions} loadingSpinner={true}
                               message='Завантаження...'/>
                    <BlankPage condition={!this.props.loadingExcursions && this.state.excursions.length === 0}
                               loadingSpinner={false} message='На жаль, нічого не вдалося знайти...'/>

                    <div className={'row mb-5 ' + (menu.length !== 0 ? 'scroll-items' : '')}>
                        {menu}
                    </div>
                    {!this.props.loadingExcursions && this.state.excursions.length > 0 &&
                    <div className='row'>
                        <div className='col-12'>
                            <PaginationComponent
                                moveFirst={this.moveFirst}
                                moveLast={this.moveLast}
                                moveNext={this.moveNext}
                                movePrev={this.movePrev}
                                moveToPage={this.moveToPage}
                                items={this.state.excursions}
                                itemsPerPage={this.state.itemsPerPage}
                                pageActive={this.state.pageActive
                                }/>
                        </div>
                    </div>}
                </div>
            </>
        );
    }
}

export default ExcursionMenuComponent;
