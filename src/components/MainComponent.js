import React, {Component} from 'react';
import ExcursionComponent from "./ExcursionComponent";
import HeaderComponent from "./HeaderComponent";
import googleAuthentication from "../service/auth";
import {googleAuthProvider} from "../service/authProviders";
import firebase from "../config/firebase-config";
import {get, getDatabase, onChildChanged, ref, set, update, onChildAdded} from "firebase/database";
import Footer from "./FooterComponent";
import {Redirect, Route, Switch, withRouter} from 'react-router-dom';
import Home from "./HomeComponent";
import ExcursionMenuComponent from "./ExcursionMenuComponent";
import Contact from "./ContactComponent";
import UserExcursionsComponent from "./UserExcursionsComponent";
import Gallery from "./GalleryComponent";
import {sortExcursionsByParam} from "./excursion_functions";

const db = getDatabase();
const refAllExcursions = ref(db, 'excursions');
const refAllUsers = ref(db, 'users');

class MainComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            excursions: [],
            clickedExcursion: null,
            userSignedIn: false,
            user: null,
            loadingExcursions: true,
            updateState: true
        };

        this.handleExcursionClicked = this.handleExcursionClicked.bind(this);
        this.checkUserSignedIn = this.checkUserSignedIn.bind(this);
        this.getUserInfoFromDB = this.getUserInfoFromDB.bind(this);
        this.addNewUserToDB = this.addNewUserToDB.bind(this);
        this.signIn = this.signIn.bind(this);
        this.signOut = this.signOut.bind(this);
        this.addExcursionsChildListener = this.addExcursionsChildListener.bind(this);
        this.loadExcursions = this.loadExcursions.bind(this);
        this.takeExcursion = this.takeExcursion.bind(this);
        this.refuseExcursion = this.refuseExcursion.bind(this);
        this.addExcursionsChildAddedListener = this.addExcursionsChildAddedListener.bind(this);

        this.handleHeaderSearch = this.handleHeaderSearch.bind(this);
    }

    addExcursionsChildAddedListener() {
        onChildAdded(refAllExcursions, data => {
            console.log('excursion child added');
            const exc = data.val();
            console.log('addExcursionsChildAddedListener  new excursion = ', exc);

            const excursions = this.state.excursions;

            excursions.push(exc);

            this.setState({
                loadingExcursions: false,
            });
        });
    }

    addExcursionsChildListener() {
        onChildChanged(refAllExcursions, data => {
            console.log('excursion updated');
            const upExc = data.val();
            console.log('addExcursionsChildListener  updated excursion = ', upExc);

            const excursions = this.state.excursions;
            const oldExc = excursions.filter((e) => e.id === upExc.id)[0];

            console.log('addExcursionsChildListener  old excursion = ', oldExc);

            Object.assign(oldExc, upExc);

            this.setState({
                updateState: true,
            });
        });
    }

    addUserChildListener() {
        onChildChanged(refAllUsers, data => {
            const upUser = data.val();

            const user = this.state.user;
            if (user === null || user.id !== upUser.id) return;
            Object.assign(user, upUser);

            this.setState({
                updateState: true,
            });
        });
    }

    loadExcursions() {
        get(refAllExcursions)
            .then(snapshot => {
                    if (!snapshot.exists()) {
                        console.log('failed to retrieve excursions!');
                        return;
                    }
                    this.setState({
                            excursions: snapshot.val(),
                            loadingExcursions: false
                        }
                    )
                }
            );
    }

    componentDidMount() {
        this.checkUserSignedIn();
        this.addExcursionsChildAddedListener();
        this.addExcursionsChildListener();
        this.addUserChildListener();
    }

    handleExcursionClicked(excursionId) {
        this.setState({
            clickedExcursion: excursionId
        });
    }

    checkUserSignedIn() {
        firebase.auth().onAuthStateChanged((user) => {
            this.setState({
                userSignedIn: user !== null
            });
            this.getUserInfoFromDB(user);
        });
    }

    getUserInfoFromDB(user) {
        if (user !== null) {
            get(ref(db, `users/${user.uid}`))
                .then((data) => {
                    if (data.exists()) {
                        this.setState({
                            user: data.val()
                        });
                    } else {
                        console.log('user does not exist');
                    }
                })
                .catch(err => {
                    console.log('Unknown error while getting a user form db');
                });
        }
    }

    addNewUserToDB(user) {
        get(ref(db, `users/${user.uid}`))
            .then((data) => {
                if (!data.exists()) {
                    console.log('user does not exist, so we\'ll add him');
                    const newUser = {
                        id: user.uid,
                        userName: user.displayName,
                        photoURL: user.photoURL,
                        excursionsRegistered: []
                    };
                    this.setState({
                        user: newUser
                    });
                    set(ref(db, `users/${user.uid}`), newUser)
                        .then(() => {
                            console.log('new user added to db');
                        });
                }
            });
    }

    signIn() {
        googleAuthentication(googleAuthProvider)
            .then((user) => {
                console.log('user signed in');
                this.addNewUserToDB(user);
            });
    }

    signOut() {
        firebase.auth().signOut()
            .then((res) => {
                console.log('logged out')
            })
            .catch((err) => {
                console.log('error while logging out')
            })
            .then(() => {
                this.setState({
                    user: null
                });
            });
    }

    takeExcursion(excursion) {
        const user = this.state.user;

        if (!user.excursionsRegistered) {
            user.excursionsRegistered = [];
        }
        user.excursionsRegistered.push(excursion.id);

        if (!excursion.user_ids) {
            excursion.user_ids = [];
        }
        excursion.user_ids.push(user.id);

        const updates = {};

        updates[`/users/${user.id}/excursionsRegistered`] = user.excursionsRegistered;

        updates[`/excursions/${excursion.id}/user_ids`] = excursion.user_ids;

        update(ref(db), updates)
            .then(data => {
                console.log(data)
            });
    }

    refuseExcursion(excursion) {
        const user = this.state.user;

        user.excursionsRegistered = user.excursionsRegistered.filter(id => id !== excursion.id);

        excursion.user_ids = excursion.user_ids.filter(id => id !== user.id);

        const updates = {};

        updates[`/users/${user.id}/excursionsRegistered`] = user.excursionsRegistered;

        updates[`/excursions/${excursion.id}/user_ids`] = excursion.user_ids;

        update(ref(db), updates)
            .then(data => {
                console.log(data)
            });
    }

    handleHeaderSearch(event) {
        event.preventDefault();
        this.props.history.push('/excursions', {searchString: event.target.elements['searchString'].value});
        event.target.reset();
        event.target.elements['searchString'].blur();
    }

    render() {
        const ExcursionWithId = ({match, history}) => {
            let pathVar = match.params.excursionId;
            console.log('path variable: ', pathVar);
            pathVar = isNaN(pathVar) ? '-1' : pathVar;
            const pathId = parseInt(pathVar, 10);
            console.log('pathId: ', pathId);
            const excursion = this.state.excursions.filter(e => e.id === pathId)[0];
            return (<ExcursionComponent
                loadingExcursions={this.state.loadingExcursions}
                takeExcursion={this.takeExcursion}
                refuseExcursion={this.refuseExcursion}
                user={this.state.user}
                pageFrom={history.location.state ? history.location.state.from : '/home'}
                excursion={excursion}/>);
        }

        const UserExcursions = ({history}) => {
            if (!this.state.user) {
                return (
                    <UserExcursionsComponent history={history} userLoggedOut={true} excursions={[]}/>
                );
            }

            const excursionsIds = this.state.user.excursionsRegistered;
            let excursions = [];
            if (excursionsIds) {
                excursions = this.state.excursions.filter(e => excursionsIds.includes(e.id));
            }
            sortExcursionsByParam(excursions, 'date_start');
            return (
                <UserExcursionsComponent userLoggedOut={false} history={history} excursions={excursions}/>
            );
        }

        return (
            <div className='App'>
                <HeaderComponent handleHeaderSearch={this.handleHeaderSearch} user={this.state.user}
                                 userSignedIn={this.state.userSignedIn} signIn={this.signIn} signOut={this.signOut}/>
                <Switch>
                    <Route exact path='/home' component={(props) => <Home history={props.history}
                                                                          loadingExcursions={this.state.loadingExcursions}
                                                                          excursions={this.state.excursions.filter(e => e.top)}/>}/>
                    <Route exact path='/excursions' component={(props) => <ExcursionMenuComponent
                        searchString={props.history.location.state ? props.history.location.state.searchString : ''}
                        history={props.history}
                        loadingExcursions={this.state.loadingExcursions}
                        userSignedIn={this.state.userSignedIn}
                        excursions={this.state.excursions}/>}/>
                    <Route path='/excursions/:excursionId' component={ExcursionWithId}/> }/>
                    <Route exact path='/myexcursions' component={UserExcursions}/>
                    <Route exact path='/gallery'
                           component={() => <Gallery loadingExcursions={this.state.loadingExcursions}
                                                     excursions={this.state.excursions}/>}/>
                    <Route exact path='/contacts' component={Contact}/> }/>
                    <Redirect to='/home'/>
                </Switch>
                <Footer/>
            </div>
        )
    };
}
export default withRouter(MainComponent);
