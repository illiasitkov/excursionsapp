import React from 'react';
import '../css/top_excursion.css'
import ExcursionCard from "./ExcursionCard";

const TopExcursionsComponent = ({excursions, history}) => {
    const menu = excursions.filter((e) => e.top).map((e) => {
            return (
                <div key={e.id} className='col-12 col-md-6 mt-5 mb-2'>
                    <ExcursionCard history={history} pageFrom='/home' excursion={e}/>
                </div>
            );
        }
    );
    return (
        <div>
            <div className='container mt-5'>
                <div className='row justify-content-center'>
                    <h1>Найпопулярніші екскурсії</h1>
                </div>
                <hr/>
                <div className='row mb-5'>
                    {menu}
                </div>
            </div>
        </div>
    );
}

export default TopExcursionsComponent;
