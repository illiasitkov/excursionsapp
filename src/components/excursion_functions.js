import * as dayjs from "dayjs";

export const expiredExcursion = (excursion) => {
    const dateStart = new Date(excursion.date_start);
    return dateStart < new Date();
}

export const excursionIsOnDate = (excursion, date) => {
    return dayjs(excursion.date_start).isSame(dayjs(date), 'day');
}

export const availableExcursion = (excursion) => {
    const user_ids = excursion.user_ids;
    const takenPlaces = user_ids ? user_ids.length : 0;
    const noFreePlaces = (excursion.max_users - takenPlaces) <= 0;
    return (!expiredExcursion(excursion) && !noFreePlaces);//new Date(excursion.date_start) >= new Date()
}

export const excursionHasString = (excursion, searchString) => {
    searchString = searchString.toLowerCase().trim();
    return excursion.caption.toLowerCase().trim().includes(searchString) ||
        excursion.text.toLowerCase().trim().includes(searchString) ||
        excursion.description.toLowerCase().trim().includes(searchString) ||
        excursion.full_text.toLowerCase().trim().includes(searchString) ||
        excursion.cities.join(',').toLowerCase().trim().includes(searchString) ||
        excursion.categories.join(',').includes(searchString) ||
        (excursion.date_start + ' ' + excursion.price.toString() + ' ' + excursion.duration.toString() + ' ' + excursion.max_users.toString())
            .includes(searchString);
}

export const sortExcursionsByParam = (excursions, sortParam, descendingOrder) => {
    return excursions.sort((e1, e2) => e1[sortParam] > e2[sortParam] ? descendingOrder ? -1 : 1 : descendingOrder ? 1 : -1);
}
